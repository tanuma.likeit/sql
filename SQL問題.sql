
--SQL練習問題

--問1
CREATE TABLE item_category(
category_id INT NOT NULL AUTO_INCREMENT
,category_name VARCHAR(256) NOT NULL
,PRIMARY KEY(category_id)
);

SELECT * FROM item_category;
DROP TABLE item_category;

--問2
CREATE TABLE item(
item_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
,item_name VARCHAR(256) NOT NULL
,item_price INT NOT NULL
,category_id INT
);

DROP TABLE item;

--問3
INSERT INTO item_category VALUES(1,'家具');
INSERT INTO item_category VALUES(2,'食品');
INSERT INTO item_category VALUES(3,'本');

--問4
INSERT INTO item VALUES(1,'堅牢な机',3000,1);
INSERT INTO item VALUES(2,'生焼け肉',50,2);
INSERT INTO item VALUES(3,'すっきりわかるJava入門',3000,3);
INSERT INTO item VALUES(4,'おしゃれな椅子',2000,1);
INSERT INTO item VALUES(5,'こんがり肉',500,2);
INSERT INTO item VALUES(6,'書き方ドリルSQL',2500,3);
INSERT INTO item VALUES(7,'ふわふわのベッド',30000,1);
INSERT INTO item VALUES(8,'ミラノ風ドリア',300,2);

--問5
SELECT item_name,item_price FROM item WHERE category_id = 1;

--問6
SELECT item_name,item_price FROM item WHERE item_price >= 1000;

--問7
SELECT item_name,item_price FROM item WHERE item_name LIKE '%肉%';

--問8
SELECT item_id,item_name,item_price,category_name FROM item LEFT OUTER JOIN item_category ON item.category_id=item_category.category_id ORDER BY item_id;

SELECT * FROM item;
SELECT * FROM item_category;

--問9
SELECT category_name,SUM(item_price) AS total_price FROM item INNER JOIN item_category ON item.category_id=item_category.category_id GROUP BY item.category_id ORDER BY total_price DESC;


